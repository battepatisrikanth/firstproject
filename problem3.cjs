const sortCarNames=function(inventory){
    let result=[];
    if(inventory == undefined ||inventory== null ||inventory.length<=0){
            return result;
        }

        let numOfCars=inventory.length;
        let sortNamesList=[];
        for(let index=0;index<numOfCars;index++){
            sortNamesList[index]=inventory[index].car_model;
        }
    result=sortNamesList.sort();
    return result;
}
module.exports=sortCarNames;
