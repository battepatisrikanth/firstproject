const carsBeforeDate=function(inventory){
    if(inventory == undefined ||inventory== null ||inventory.length<=0){
        return [];
    }
        let numOfCars=inventory.length;
        let carYearsList=[];
        for(let index=0;index<numOfCars;index++){
            carYearsList[index]=inventory[index].car_year;
        }
        let beforeDateCars=[];
        let newIndex=0;
        for(index=0;index<numOfCars;index++){
            if(carYearsList[index]<2000){
                beforeDateCars[newIndex]=carYearsList[index];
                newIndex++;
            }
        }
        return beforeDateCars;



    }
module.exports=carsBeforeDate;
