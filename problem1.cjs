const findCarDetails = (inventory,id)=> {
    let result=[];
    if(inventory == null || inventory == undefined ||id==null||id ==undefined ||id<0){
        return result;
    }   
    if(!Array.isArray(inventory)){
        return result;
    }
    if (typeof id!= 'number'){
        return result;
    }
        
    for (let index=0;index<inventory.length;index++){
        if(inventory[index].id==id){
            result.push(inventory[index]);
            return result;
        }
    }
    
}



module.exports=findCarDetails;
