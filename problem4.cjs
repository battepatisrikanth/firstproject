const getCarYears=function(inventory){
    let carYearsList=[];
    if(inventory == undefined ||inventory== null ||inventory.length<=0){
        return carYearsList;
    }
        let numOfCars=inventory.length;
        
        for(let index=0;index<numOfCars;index++){
            carYearsList[index]=inventory[index].car_year;
        }
        return carYearsList;
    }
module.exports=getCarYears;
